require 'test_helper'
require 'selenium-webdriver'
require 'capybara/rails'

class UserFlowsTest < ActionDispatch::IntegrationTest

  include Capybara::DSL

  def teardown
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end

  Capybara.register_driver :remote_firefox do |app|
    Capybara::Selenium::Driver.new(app, {browser: :remote})
  end

  Capybara.default_driver = :remote_firefox
  Capybara.app_host = 'http://localhost:3000'

  test 'create_user' do
    visit('/users')
    assert_equal('Listing users', find(:css, 'h1').text)
    click_link 'New User'
    fill_in('Name', :with => 'nnn')
    fill_in('Password', :with => 'ppp')
    click_button 'Create User'


  end
  # test "the truth" do
  #   assert true
  # end
end
