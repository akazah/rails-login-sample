require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test 'initialize with no params' do
    assert User.new
  end
  test 'initialize with params' do
    assert User.new({name: 'name', password: 'pass'})
  end

  test 'password check' do
    assert User.new({name: 'name', password: 'pass'}).check? User.new({name: 'name2', password: 'pass'}).password
  end

end
