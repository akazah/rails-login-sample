require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  fixtures :users
  setup do

  end
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:users)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get login" do
    get :login
    assert_response :success
  end

  test "should get auth" do
    post(:login, :user => {:name => 'testname', :password => 'pass'})
    assert_response :success
  end

  test "should get edit" do
    get(:edit, {id: User.all.first.id})
    assert_response :success
  end

  test "should get show" do
    get(:show, {id: User.all.first.id})
    assert_response :success
  end
  test "shoud create new user" do
    assert_difference('User.count', 1) {
      post(:create, {:user => {name: User.all.first.name, password: User.all.first.password}})
    }

  end
  test "shound update a user" do
    assert_difference('User.all.first.name', 'mod') {
      put(:update, {id: User.all.first.id, :user => {name: "#{User.all.first.name}mod", password: 'pass'}})
    }
  end
  test "should delete a user" do
    assert_difference('User.count', -1) {
      delete(:destroy, {id: User.all.first.id})
    }

  end

end
