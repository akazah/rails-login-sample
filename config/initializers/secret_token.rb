# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
RailsLogin::Application.config.secret_key_base = 'fe1ad4f091a2a82db9403dc7a8e4820fb53d2d9b279c5c9d0044df2dfb7543af6b882fb215b5e94ce91146191472c3c752d4b8bbc37801cf3ced8a0d98fa61e8'
