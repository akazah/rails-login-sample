class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  before_action :login_check

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    p user_options, user_params
    @user = User.new(user_params, user_options)
    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render action: 'show', status: :created, location: @user }
      else
        @user.password = ''
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

  # GET  /users/login
  # POST /users/login
  def login

    @user = User.new
  end

  # PATCH /users/login
  # PUT /user/slogin
  def auth
    @user = auth_check
    p @user
    respond_to do |format|
      if !@user.errors.any? && @user.set_login_token(session) then
        format.html { redirect_to @user, notice: 'User has successfully login.' }
        format.json { head :no_content }
      else
        format.html { render action: 'login' }
        format.json { render json: @user.errors, status: :unprocessable_login }
      end
    end
  end

  # GET /users/logout
  def logout
    session['logintoken'] = ''
    redirect_to '/users'
  end

  private

  def auth_check
    user = User.new(user_params)
    #user.password_confirmation = user.password
    if !user.valid?
      return user
    end
    target = User.where(name: user.name).first
    if target
      user.password
      if target.check? user.password
        return target
      else
        user.errors.add(:AuthError, 'badpass')
        return user
      end
    else
      user.errors.add(:AuthError, 'nouser')
      return user
    end
  end


  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:name, :password)
  end

  def user_options
    params.require(:user).permit(:mode)
  end

  def login_check
    if session['logintoken'].nil?
      session['logintoken'] = ''
    end
    if session['logintoken'].length > 0 then
      target = User.where(:logintoken => session['logintoken'])
      if target then
        @current = target.first
      else
        return false
      end
    end
  end

end
