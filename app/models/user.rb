require 'digest/sha1'
require 'time'

class User < ActiveRecord::Base

  validates :name, presence: true
  validates :password, :presence => true

  def initialize(attributes = {}, options = {})
    super

    if attributes.keys.include?(:name) then
      self.salt ||= options['salt'] || Time.now.to_i.to_s
      self.password = User.hash attributes['password'], self.salt
    end
  end

  def self.hash(password, salt)
    Digest::SHA1.hexdigest("#{password}#{salt}");
  end

  def set_login_token session
    begin
      token = Digest::SHA1.hexdigest(Time.now.to_i.to_s)
    end while (User.where(:logintoken => token).count.nil?)
    self.logintoken = token
    if self.save
      session[:logintoken] = token
      return true
    else
      return false
    end
  end


  def check?(password)
    self.password == password
  end


end
